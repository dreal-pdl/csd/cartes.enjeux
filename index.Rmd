--- 
title: "Cartes d'enjeux DREAL Pays de la Loire `r params$annee`"
author: "DREAL PDL/SCTE/CSD"
date: "`r Sys.Date()`"
description: "Cartes d'enjeux DREAL Pays de la Loire `r params$annee`"
site: bookdown::bookdown_site
documentclass: book
params:
  annee: "2024"
---


# Introduction {.unnumbered}

Ce document rassemble différentes cartes illustrant les enjeux de la région Pays de la Loire en `r params$annee` pour ce qui concerne l'environnement, l'aménagement et le logement.


```{r library, include=FALSE}
library(sf)
library(tidyverse)
library(gouvdown)
library(mapfactory)
library(COGiter)
library(datalibaba)

# thèmes ggplot
theme_carto <- gouvdown::theme_gouv_map(plot_title_size = 17, subtitle_size = 15, base_size = 13, caption_size = 11)
theme_graph <- gouvdown::theme_gouv(plot_title_size = 14, subtitle_size  = 12, base_size = 10, caption_size = 10) +
                     ggplot2::theme(plot.caption.position =  "plot")
# thème carto par defaut
ggplot2::theme_set(theme_carto)

# options globales des chuncks
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, error = FALSE, message = FALSE, progress = FALSE, cache = FALSE)

# # fond carto stat de la région
# fond_carto <- fond_carto(nom_reg = "Pays de la Loire")

```


1- Principales caractéristiques des Pays de la Loire dans les domaines d’intervention de la DREAL

La région Pays de la Loire est caractérisée par un territoire globalement très dynamique tant sur le plan
démographique qu’économique, cependant contrasté entre une frange littorale et un département
Loire-Atlantique attractifs et une partie plus rurale en stagnation. Il présente d’importantes
implantations industrielles sur l’estuaire de la Loire, une grande diversité de milieux naturels, de
paysages et d’espèces et une ressource en eau dégradée en quantité épisodiquement insuffisante pour
satisfaire tous les usages. Il est exposé à des risques importants d’inondation et de submersion marines
et est nettement déficitaire en termes de production d’énergie. Depuis l’abandon de l’aéroport du
Grand Ouest à Notre Dame des Landes, qui a ravivé la crainte du déclassement de la région Pays de la
Loire par rapport à ses voisines, le territoire n’accueille aucun grand projet d’infrastructure de transport
susceptible d’aboutir à court ou moyen terme.
Les enjeux qui en résultent dans le domaine de la DREAL vont par conséquent porter sur le
renforcement de l’offre de logement, la densification de l’habitat et la décarbonation des mobilités du
quotidien, la gestion économe des différentes ressources soumises à une forte demande (eau, espaces,
etc.) ; la restauration de la qualité de l’eau ; le développement des énergies renouvelables ; la sécurité
des installations industrielles, en particulier sur la zone industrielle de Montoir-de-Bretagne ; la
prévention des risques naturels littoraux et fluviaux ; la réduction du temps d’accès à l’Ile de France.
